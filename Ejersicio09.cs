﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejersicio09
{
    class Program
    {
        public void Multiplicar()
        {
            int r = 0;
            Console.WriteLine("Ingresa un numenro:");
            int mult = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("");
            if (mult > 0)
            {

                for(int i=0; i <= 12; i++)
                {
                    r = mult * i;
                    Console.WriteLine("{0} X {1} = {2}", mult, i, r);
                }
            }
            else
            {
                System.Environment.Exit(-1);
            }

        }

        static void Main(string[] args)
        {
            Program p = new Program();
            p.Multiplicar();
            Console.ReadKey();
        }
    }
}
