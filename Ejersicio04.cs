﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejersicio04
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Un programa que pida al usuario 10 números y luego calcule y muestre cuál es el mayor de todos ellos.*/
            

            float[] num = new float[10];
            float mayor = 0, menor = 0;
            int flag = 0;

            for (int i = 0; i < 10; i++)
            {
                Console.Write("Ingrese {0}º número: ", i + 1);
                num[i] = float.Parse(Console.ReadLine());

                if (flag == 0)
                {
                    mayor = num[i];
                    menor = num[i];
                    flag = 1;
                }
                else
                {
                    if (num[i] > mayor)
                        mayor = num[i];
                    
                }
            }
           
                Console.Write("\nEl mayor de los 10 números es el {0}", mayor);

            Console.Read();



        }
    }
}
