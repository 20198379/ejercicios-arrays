﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejersicio10
{
    class Program
    {
        public void persona()
        {
            string nombre = "";
            int edad = 0;
            int com = 0;
            Console.WriteLine(" ingrese el nombre: ");
            nombre = Console.ReadLine();
            Console.WriteLine("introduzca la edad: ");
            edad = Convert.ToInt32(Console.ReadLine());

            if (edad >= 18) 
            {
                Console.WriteLine("la persona {0} es mayor de edad {1}", nombre,edad);
            }
            else
            {
                Console.WriteLine("la persona {0} es menor de edad {1}", nombre, edad);
            }
        }
    
        static void Main(string[] args)
        {
            Program bobo = new Program();
            bobo.persona();
            Console.ReadKey();
        }
    }
}
