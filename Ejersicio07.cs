﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejersicio06
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Un programa que te pida tu nombre y lo muestre en pantalla separando cada letra de la siguiente con un espacio.
             * Por ejemplo, si tu nombre es "Juan", debería aparecer en pantalla "J u a n".*/
            String texto = "", le = ""; 
            int i = 0;


            Console.Write("Ingresa una cadena de texto a deletrear: ");
            texto = Console.ReadLine();

            for (i = 0; i <= texto.Length - 1; i++)
            {
                le = texto.Substring(i, 1);
                Console.WriteLine("\nLetra de posición " + i + " = " + le);
                
            }

            Console.WriteLine("\n\nEL total de caracteres es " + texto.Length + " letras ");
            Console.ReadKey();
        }
    }
}
