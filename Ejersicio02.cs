﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array02
{
    class Program
    {
        static void Main(string[] args)
        {
            /*2.Un programa que pida al usuario 5 números reales(pista: necesitarás un array de "float")
             * y luego los muestre en el orden contrario al que se introdujeron.*/

            float[]arr = new float[5];
            int sc;
            for(int i=0;i<arr.Length; i++)
            {
                Console.WriteLine("introduzca un numero ");
                arr[i] = Convert.ToInt32(Console.ReadLine());

            }
            Console.WriteLine("los numeros en orden inverso son: ");
            int j= 4;
            while (j >= 0)
            {
                Console.WriteLine("| "+arr[j]+" |\n");
                j--;
            }
            Console.ReadKey();
        }
    }
}
