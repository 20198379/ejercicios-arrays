﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejersicio05
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Un programa que prepare espacio para un máximo de 100 nombres. 
             * El usuario deberá ir introduciendo un nombre cada vez, hasta que se pulse Intro sin teclear nada, 
             * momento en el que dejarán de pedirse más nombres y se mostrará en pantalla la lista de los nombres que se han introducido.*/
            string[] nombre = new string[100];
            int contar = 0;

            for(int i=0; i<100; i++)
            {
                Console.WriteLine("introduca un nombre: ");
                nombre[i] = Console.ReadLine();
            }
            while (contar < 100)
            {
                Console.WriteLine("lista de nombre {0} ",nombre[contar]);
                contar++;
            }
            Console.ReadKey();
        }
    }
}
