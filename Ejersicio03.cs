﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejersicio03
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Un programa que almacene en un array el número de días que tiene cada mes (supondremos que es un año no bisiesto),
             * pida al usuario que le indique un mes (1=enero, 12=diciembre) y muestre en pantalla el número de días que tiene ese mes.*/
            int dia;
            int[] arr = new int[13] {31,31,28,30,31,30,31,30,31,30,31,30,31 };

            Console.WriteLine("introduca el mes deseado:");
            dia = Convert.ToInt32(Console.ReadLine());
            dia = arr[dia];
            

            Console.WriteLine("| tu mes tiene "+ dia+" dias |");
           
            Console.ReadKey();
        }
    }
}
